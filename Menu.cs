﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppointmentSystem
{
    // Contains all functions for displaying menus, receiving user choices and calling the major application functions
    class Menu
    {
        public enum MainMenuOptions { LIST_ROOMS = 1, LIST_SLOTS, STAFF_MENU, STUDENT_MENU, EXIT }
        public enum StaffMenuOptions { LIST_STAFFS = 1, ROOM_AVAILABILITY, CREATE_SLOT, REMOVE_SLOT, EXIT }
        public enum StudentMenuOptions { LIST_STUDENTS = 1, STAFF_AVAILABILITY, MAKE_BOOKING, CANCEL_BOOKING, EXIT }

        void displayWelcome()
        {
            Console.WriteLine("--------------------------------------------------------------------------------");
            Console.WriteLine("Welcome to Appointment Scheduling and Reservation System");
            Console.WriteLine("--------------------------------------------------------------------------------");
        }

        void displayMenu()
        {
            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------------------------------------");
            Console.WriteLine("Main menu:");
            Console.WriteLine("    1. List rooms");
            Console.WriteLine("    2. List slots");
            Console.WriteLine("    3. Staff menu");
            Console.WriteLine("    4. Student menu");
            Console.WriteLine("    5. Exit");
            Console.WriteLine();
        }

        void displayStaffMenu()
        {
            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------------------------------------");
            Console.WriteLine("Staff menu:");
            Console.WriteLine("    1. List staffs");
            Console.WriteLine("    2. Room availability");
            Console.WriteLine("    3. Create Slot");
            Console.WriteLine("    4. Remove Slot");
            Console.WriteLine("    5. Exit");
            Console.WriteLine();
        }

        void displayStudentMenu()
        {
            Console.WriteLine();
            Console.WriteLine("--------------------------------------------------------------------------------");
            Console.WriteLine("Student menu:");
            Console.WriteLine("    1. List students");
            Console.WriteLine("    2. Staff availability");
            Console.WriteLine("    3. Make booking");
            Console.WriteLine("    4. Cancel Booking");
            Console.WriteLine("    5. Exit");
            Console.WriteLine();
        }

        public void runMenu(ASREngine engine)
        {
            int? choice;

            displayWelcome();

            do
            {
                displayMenu();
                Console.Write("Enter option: ");
                choice = getInt(1, 5);

                switch (choice)
                {
                    case (int)MainMenuOptions.LIST_ROOMS:
                        engine.getSlotList().displayRooms();
                        break;
                    case (int)MainMenuOptions.LIST_SLOTS:
                        engine.getSlotList().listSlots();
                        break;
                    case (int)MainMenuOptions.STAFF_MENU:
                        runStaffMenu(engine);
                        break;
                    case (int)MainMenuOptions.STUDENT_MENU:
                        runStudentMenu(engine);
                        break;
                    case (int)MainMenuOptions.EXIT:

                    default:
                        break;
                }
            } while (choice != 5);

        }

        private void runStaffMenu(ASREngine engine)
        {
            int? choice;

            do
            {
                displayStaffMenu();
                Console.Write("Enter option: ");
                choice = getInt(1, 5);

                switch (choice)
                {
                    case (int)StaffMenuOptions.LIST_STAFFS:
                        engine.getUserList().ToString();
                        break;
                    case (int)StaffMenuOptions.ROOM_AVAILABILITY:
                        engine.getSlotList().displayAvailability();
                        break;
                    case (int)StaffMenuOptions.CREATE_SLOT:
                        engine.getSlotList().createSlot(engine);
                        break;
                    case (int)StaffMenuOptions.REMOVE_SLOT:
                        engine.getSlotList().removeSlot(engine);
                        break;
                    case (int)StaffMenuOptions.EXIT:

                    default:
                        break;
                }
            } while (choice != 5);
        }

        private void runStudentMenu(ASREngine engine)
        {
            int? choice;

            do
            {
                displayStudentMenu();
                Console.Write("Enter option: ");
                choice = getInt(1, 5);

                switch (choice)
                {
                    case (int)StudentMenuOptions.LIST_STUDENTS:
                        engine.getUserList().displayStudents();
                        break;
                    case (int)StudentMenuOptions.STAFF_AVAILABILITY:
                        engine.getSlotList().staffAvailability(engine);
                        break;
                    case (int)StudentMenuOptions.MAKE_BOOKING:
                        engine.getSlotList().makeBooking(engine);
                        break;
                    case (int)StudentMenuOptions.CANCEL_BOOKING:
                        engine.getSlotList().cancelBooking(engine);
                        break;
                    case (int)StudentMenuOptions.EXIT:

                    default:
                        break;
                }
            } while (choice != 5);

        }

        string getString()
        {
            return Console.ReadLine();
        }

        // Sanitises the input for the menu
        int? getInt(int min, int max)
        {
            int result;

            string input = Console.ReadLine();
            if (int.TryParse(input, out result))
            {
                if (result < min || result > max)
                {
                    Console.WriteLine("Please enter a number from {0} to {1}!", min, max);
                }

                return result;
            }
            Console.WriteLine("Input was not a valid number!");
            return null;
        }
    }
}
