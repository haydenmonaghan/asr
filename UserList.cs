﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace AppointmentSystem
{
    // Contains functions for accessing, creating, modifying, querying and removing users
    class UserList : IEnumerable
    {
        List<User> users = new List<User>();

        public void add(User newUser)
        {
            users.Add(newUser);
        }

        public IEnumerator GetEnumerator()
        {
            foreach (User user in users)
            {
                yield return user;
            }
        }

        // Prevent duplicate users
        public bool userExists(string userID)
        {
            var theUser =
                (from u in users
                 where u.id == userID
                 select u);

            if (theUser.Count() == 0)
            {
                return true;
            }

            Console.WriteLine("Duplicate user id in users.txt file.");
            return false;
        }

        public User getUser(string userID)
        {
            User user =
                (from u in users
                 where u.id == userID
                 select u)
                .First();

            if (!user.Equals(null))
            {
                return user;
            }

            return null;
        }

        // List all staff
        public void displayStaff()
        {
            Console.WriteLine("Existing Staff:");
            Console.Write("".PadLeft(4) + "{0, -15}", "Name");
            Console.Write("{0, -15}", "ID");
            Console.Write("{0, -15}", "Email");
            Console.WriteLine();

            var staff =
                from u in users
                where u.userType == User.UserType.Staff
                select u;

            foreach (User user in staff)
            {
                Console.WriteLine("".PadLeft(4) + user.ToString());
            }
        }

        // List all students
        public void displayStudents()
        {
            Console.WriteLine("Existing Students:");
            Console.Write("".PadLeft(4) + "{0, -15}", "Name");
            Console.Write("{0, -15}", "ID");
            Console.Write("{0, -15}", "Email");
            Console.WriteLine();

            var students =
                from u in users
                where u.userType == User.UserType.Student
                select u;

            foreach (User user in students)
            {
                Console.WriteLine("".PadLeft(4) + user.ToString());
            }
        }

        public override string ToString()
        {
            displayStaff();
            return base.ToString();
        }
    }
}
