﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppointmentSystem
{
    class User
    {
        public enum UserType { Staff, Student }

        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public UserType userType { get; set; }

        public User (string id, string name, string email, UserType userType)
        {
            this.id = id;
            this.name = name;
            this.email = email;
            this.userType = userType;
        }        
        
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0, -15}{1, -15}{2, -15}", name, id, email);
            return sb.ToString();
        }
    } 
}
