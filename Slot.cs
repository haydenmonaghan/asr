﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppointmentSystem
{
    class Slot
    {
        public enum Room { A, B, C, D }

        public Room room { get; set; }
        public DateTime time { get; set; }
        public User staff { get; set; }
        public User booking { get; set; }

        public Slot(Room room, DateTime time, User staff)
        {
            this.room = room;
            this.time = time;
            this.staff = staff;
            booking = null;
        }

        public void addBooking(User student)
        {
            booking = student;
        }

        public void cancelBooking()
        {
            this.booking = null;
        }

        public override string ToString()
        {
            TimeSpan slotDuration = new TimeSpan(1, 0, 0);
            DateTime endTime = time + slotDuration;
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("{0, -15}", room.ToString());
            sb.AppendFormat("{0, -15}", time.ToShortDateString());
            sb.AppendFormat("{0, -15}", endTime.ToShortDateString());
            sb.AppendFormat("{0, -15}", staff.id);
            sb.AppendFormat("{0, -15}", (booking == null ? "No booking" : booking.id));

            return sb.ToString();
        }

        public string slotDetails()
        {
            DateTime endTime = time + new TimeSpan(01,00,00);
            return time.ToShortTimeString() + " - " + endTime.ToShortTimeString() + " " + staff.id;
        }

        // Different formatting for different functions
        public string slotRoomStartEnd()
        {
            DateTime endTime = time + new TimeSpan(01,00,00);
            return "Room " + room.ToString() + " " + time.ToShortTimeString() + " - " + endTime.ToShortTimeString();
        }
    }
}
