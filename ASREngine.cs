﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace AppointmentSystem
{
    // Contains functions for loading/validating the user data, holds the containers for users and slots.
    class ASREngine
    {
        enum UserDataType { Name, Id, Email };

        UserList users = new UserList();
        SlotList slots = new SlotList();

        public SlotList getSlotList()
        {
            return slots;
        }       

        public UserList getUserList()
        {
            return users;
        }

        public void start(ASREngine engine)
        {
            if (loadData())
            {
                Menu menu = new Menu();
                menu.runMenu(engine);
            }            
        }
        
        public bool loadData()
        {
            try
            {
                using (StreamReader streamReader = new StreamReader("users.txt"))
                {
                    string currentLine;

                    while ((currentLine = streamReader.ReadLine()) != null)
                    {
                        string[] parts = currentLine.Split(',');

                        if (validateID(parts[(int)UserDataType.Id]) &&
                            validateEmail(parts[(int)UserDataType.Id], parts[(int)UserDataType.Email]) &&
                            users.userExists(parts[(int)UserDataType.Id]))
                        {
                            User newUser = createUser(parts[(int)UserDataType.Name],
                                                 parts[(int)UserDataType.Id],
                                                 parts[(int)UserDataType.Email]);                           

                            users.add(newUser);
                        }
                        else
                        {
                            // Invalid data
                            return false;
                        }
                    }                             

                    return true;
                }
            }

            catch (FileNotFoundException e)
            {
                Console.WriteLine("The users.txt file could not be found.");
                Console.WriteLine(e.Message);
            }

            catch (Exception e)
            {
                Console.WriteLine("The users.txt file could not be read.");
                Console.WriteLine(e.Message);
            }

            return false;
        }

        bool validateID(string id)
        {
            int idNumber;

            // ID must must be of 8 character length
            if (id.Length != 8)
            {
                Console.WriteLine("Invalid ID in users.txt file. Id must be 8 characters in length.");

                return false;
            }

            // ID must start with either 'e' or 's'    
            else if (id[0] != 'e' && id[0] != 's')
            {
                Console.WriteLine("Invalid ID in users.txt file. Id must start with an 'e' or 's'");
                return false;
            }

            // The number portion of the ID must be a valid number
            else if (!int.TryParse(id.Substring(1, 6), out idNumber))
            {
                Console.WriteLine("Invalid ID in users.txt file. The number portion must be a valid number.");
                return false;
            }

            return true;
        }

        bool validateEmail(string id, string email)
        {
            //  Must end with either @ems.rmit.edu.au or @student.rmit.edu.au
            Regex staffCheck = new Regex("@ems.rmit.edu.au");

            if (id[0] == 'e' && staffCheck.IsMatch(email))
            {
                return true;
            }

            Regex studentCheck = new Regex("@student.rmit.edu.au");
            if (studentCheck.IsMatch(email))
            {
                return true;
            }

            Console.WriteLine("Invalid email in users.txt file.");
            return false;
        }        

        User createUser(string name, string id, string email)
        {
            // Work out if it is a staff or student
            User.UserType ut = id[0] == 'e'? User.UserType.Staff:User.UserType.Student;

            User newUser = new User(id, name, email, ut);

            return newUser;
        }
                
    }    
}
