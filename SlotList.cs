﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;

namespace AppointmentSystem
{
    // Contains functions for accessing, creating, modifying, querying and removing slots
    class SlotList : IEnumerable
    {
        List<Slot> slots = new List<Slot>();
        TimeSpan start = new TimeSpan(9, 0, 0);
        TimeSpan end = new TimeSpan(14, 0, 0);
        TimeSpan slotDuration = new TimeSpan(1, 0, 0);
        const int MAX_SLOTS_PER_DAY = 4;
        const int MAX_SLOTS_PER_ROOM = 2;

        // Checks the availability of all rooms on a given day
        public void displayAvailability()
        {
            DateTime date;
            Console.WriteLine("Room availability");
            Console.WriteLine("Enter a date to check room availability (dd-mm-yyyy):");

            if (DateTime.TryParse(Console.ReadLine(), out date))
            {
                listSlots(date, Slot.Room.A);
                listSlots(date, Slot.Room.B);
                listSlots(date, Slot.Room.C);
                listSlots(date, Slot.Room.D);
            }

            else
            {
                Console.WriteLine("Invalid date.");
            }
        }

        // A student cannot make more than one booking a day
        public bool checkNumberOfBookings(ASREngine engine, Slot slot, User student)
        {
            var dayBookings =
                from s in slots
                where s.booking == student
                select s;

            if (dayBookings.Count() > 0)
            {
                return false;
            }

            return true;
        }

        public bool makeBooking(ASREngine engine)
        {
            DateTime date;
            TimeSpan time;
            DateTime dateTime;
            Slot.Room roomName;
            Slot slot;
            User student;
            User staff;
            string staffID;
            string studentID;

            Console.WriteLine("Make booking");
            Console.Write("Enter date for slot (dd-mm-yyyy): ");

            if (!DateTime.TryParse(Console.ReadLine(), out date))
            {
                Console.WriteLine("Invalid date.");
                return false;
            }

            Console.Write("Enter time for slot (hh:mm): ");

            if (!TimeSpan.TryParse(Console.ReadLine(), out time))
            {
                Console.WriteLine("Invalid time");
                return false;
            }

            dateTime = date + time;

            Console.Write("Enter staff ID:");
            staffID = Console.ReadLine();

            Regex staffIdRegex = new Regex(@"^(e\d{7})$");

            if (!staffIdRegex.IsMatch(staffID))
            {
                Console.WriteLine("Invalid staff ID.");
                return false;
            }

            Console.Write("Enter room name: ");

            if (!Enum.TryParse(Console.ReadLine(), out roomName))
            {
                Console.WriteLine("Invalid room name.");
                return false;
            }

            Console.Write("Enter student ID: ");
            studentID = Console.ReadLine();

            Regex studentIdRegex = new Regex(@"^(s\d{7})$");

            if (!studentIdRegex.IsMatch(studentID))
            {
                Console.WriteLine("Invalid student ID.");
                return false;
            }

            staff = engine.getUserList().getUser(staffID);

            if (staff == null)
            {
                Console.WriteLine("Staff member not found.");
            }

            slot = getSlot(roomName, dateTime, staff);

            student = engine.getUserList().getUser(studentID);

            if (student == null)
            {
                Console.WriteLine("Student not found.");
                return false;
            }

            // Only one student can book a slot
            if (slot.booking != null)
            {
                Console.WriteLine("Slot is already fully booked.");
                return false;
            }

            // Student can only make one booking a day
            if (!checkNumberOfBookings(engine, slot, student))
            {
                Console.WriteLine("Students can only make one booking per day.");
                return false;
            }

            slot.addBooking(student);

            Console.WriteLine("\nSlot booked successfully");
            return true;

        }

        public bool cancelBooking(ASREngine engine)
        {
            Console.WriteLine("Cancel booking");

            int counter = 1;
            string choiceString;
            int choice;

            var bookedSlots =
                from s in slots
                where s.booking != null
                select s;

            foreach (Slot slot in bookedSlots)
            {
                Console.WriteLine("{0}. {1}", counter++, slot.ToString());
            }

            Console.WriteLine("0. Cancel");
            Console.Write("Which booking would you like to remove? Enter the number: ");

            choiceString = Console.ReadLine();

            if (!int.TryParse(choiceString, out choice))
            {
                Console.WriteLine("Invalid option.");
                return false;
            }

            if (choice == 0)
            {
                return false;
            }

            slots[choice - 1].cancelBooking();

            Console.WriteLine("Booking successfully removed");
            return true;

        }

        public Slot getSlot(Slot.Room room, DateTime time, User staff)
        {
            Slot slot = null;

            var allSlots =
                from s in slots
                where s.room == room && (s.time.CompareTo(time) == 0) && s.staff.id == staff.id
                select s;

            if (allSlots.Count() > 0)
            {
                slot = allSlots.First();
                return slot;
            }

            return null;

        }

        public void displayRooms()
        {
            Array rooms = Enum.GetValues(typeof(Slot.Room));

            Console.WriteLine("Existing rooms:");
            Console.WriteLine("    Room Name");

            foreach (Slot.Room room in rooms)
            {
                Console.WriteLine("    {0}", Enum.GetName(typeof(Slot.Room), room));
            }
        }

        public void add(Slot newSlot)
        {
            slots.Add(newSlot);
        }

        public bool listSlots()
        {
            DateTime date;

            Console.WriteLine("Existing slots");
            Console.WriteLine("Enter date for slots (dd-mm-yyyy):");
            Console.WriteLine();

            if (!DateTime.TryParse(Console.ReadLine(), out date))
            {
                Console.WriteLine("Invalid date entered.");
                return false;
            }

            var daySlots =
                from s in slots
                where s.time.Date == date.Date
                select s;

            if (daySlots.Count() > 0)
            {
                Console.WriteLine("Existing slots on {0}:", date.ToShortDateString());
                Console.Write("".PadLeft(4) + "{0, -15}", "Room Name");
                Console.Write("{0, -15}", "Start Time");
                Console.Write("{0, -15}", "End Time");
                Console.Write("{0, -15}", "Staff ID");
                Console.Write("{0, -15}", "Bookings");
                Console.WriteLine();

                foreach (Slot slot in daySlots)
                {
                    Console.WriteLine("".PadLeft(4) + slot.ToString());
                }

                return true;
            }

            Console.WriteLine("No existing slots for this day.");
            return true;
        }

        public bool listSlots(DateTime date, Slot.Room room)
        {
            Console.WriteLine("Room {0}", Enum.GetName(typeof(Slot.Room), room));
            var roomSlots =
                from s in slots
                where s.time.Date == date.Date && s.room == room
                select s;

            foreach (Slot slot in roomSlots)
            {
                Console.WriteLine(slot.slotDetails());
            }

            switch (roomSlots.Count())
            {
                case 0:
                    Console.WriteLine("2 Slots available.");
                    break;
                case 1:
                    Console.WriteLine("1 Slot available.");
                    break;
                default:
                    Console.WriteLine("Room fully booked, no slots available");
                    break;
            }

            Console.WriteLine();

            return true;
        }

        // Lists the availabilty of a staff member on a given date
        public bool staffAvailability(ASREngine engine)
        {
            DateTime date;
            string staff;
            User staffMember;

            Console.WriteLine("Staff Availability");
            Console.Write("Enter date for staff availability (dd-mm-yyyy): ");

            if (!DateTime.TryParse(Console.ReadLine(), out date))
            {
                Console.WriteLine("Invalid date.");
                return false;
            }

            Console.Write("Enter staff ID: ");

            staff = Console.ReadLine();

            Regex staffIdRegex = new Regex(@"^(e\d{7})$");

            if (!staffIdRegex.IsMatch(staff))
            {
                Console.WriteLine("Invalid staff ID.");
                return false;
            }

            staffMember = engine.getUserList().getUser(staff);

            if (staffMember == null)
            {
                Console.WriteLine("User not found.");
                return false;
            }

            listSlots(date, staffMember);

            return true;
        }

        private void listSlots(DateTime date, User staffMember)
        {
            var staffSlots =
                from s in slots
                where s.time.Date == date.Date && s.staff.id == staffMember.id && s.booking == null
                select s;

            Console.WriteLine("Staff {0} availability on {1}:", staffMember.id, date.ToShortDateString());

            foreach (Slot slot in slots)
            {
                Console.WriteLine(slot.slotRoomStartEnd());
            }

        }

        public IEnumerator GetEnumerator()
        {
            foreach (Slot slot in slots)
            {
                yield return slot;
            }
        }

        // So that a room cannot be double booked at the same or overlapping time slots
        public bool checkAvailability(Slot newSlot)
        {
            var daySlots =
                from s in slots
                where s.time.Date == newSlot.time.Date && s.room == newSlot.room
                select s;

            foreach (var existingSlot in daySlots)
            {
                if (existingSlot.time.TimeOfDay <= newSlot.time.TimeOfDay &&
                    (existingSlot.time.TimeOfDay + slotDuration) >= newSlot.time.TimeOfDay)
                {
                    Console.WriteLine("This room is already booked at this time slot.");
                    return false;
                }
            }

            return true;
        }

        public bool checkTimeSlot(DateTime dateTime)
        {
            TimeSpan slotTime = dateTime.TimeOfDay;
            // The staff don't work weekends
            if (dateTime.DayOfWeek == DayOfWeek.Sunday || dateTime.DayOfWeek == DayOfWeek.Saturday)
            {
                Console.WriteLine("Unable to create weekend slots.");
                return false;
            }

            // 9:00am - 3:00 pm enforced
            if (slotTime < start || slotTime > end)
            {
                Console.WriteLine("Slots must run inbetween 9:00 and 3:00");
                return false;
            }

            // Prevents slots from being created in the past
            if (dateTime < DateTime.Now)
            {
                Console.WriteLine("Slots can only be created in the future.");
                return false;
            }

            return true;
        }

        // Prevent staff from making more than the allowed number of slots per day
        public bool checkNumberOfBookings(Slot newSlot)
        {
            var daySlots =
                from s in slots
                where s.time.Date == newSlot.time.Date && s.staff == newSlot.staff
                select s;

            int slotCount = daySlots.Count();

            // Not allowed more than a certain number slots a day per staff
            if (slotCount >= MAX_SLOTS_PER_DAY)
            {
                Console.WriteLine("Cannot create more than {0} slots per staff per day.", MAX_SLOTS_PER_DAY);
                return false;
            }
            else
            {
                return true;
            }
        }

        // Prevent a room being used mre than the allowed number of times per day
        public bool checkNumberOfRoomBookings(Slot newSlot)
        {
            var roomSlots =
                from s in slots
                where s.time.Date == newSlot.time.Date && s.room == newSlot.room
                select s;

            int roomSlotCount = roomSlots.Count();

            // Not allowed more than a certain number slots a day per staff
            if (roomSlotCount >= MAX_SLOTS_PER_ROOM)
            {
                Console.WriteLine("Cannot create more than {0} slots per room per day.", MAX_SLOTS_PER_ROOM);
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool createSlot(ASREngine engine)
        {
            DateTime date;
            TimeSpan time;
            String staffID;
            Regex staffIdRegex;
            Slot.Room roomName;
            DateTime dateTime;
            Slot newSlot;

            Console.WriteLine("Create slot");
            Console.Write("Enter date for slot (dd-mm-yyyy):");

            if (!DateTime.TryParse(Console.ReadLine(), out date))
            {
                Console.WriteLine("Invalid date.");
                return false;
            }

            Console.Write("Enter time for slot (hh:mm): ");

            if (!TimeSpan.TryParse(Console.ReadLine(), out time))
            {
                Console.WriteLine("Invalid time");
                return false;
            }

            dateTime = date + time;

            if (!checkTimeSlot(dateTime))
            {
                return false;
            }


            Console.Write("Enter staff ID: ");
            staffID = Console.ReadLine();

            staffIdRegex = new Regex(@"^(e\d{7})$");

            if (!staffIdRegex.IsMatch(staffID))
            {
                Console.WriteLine("Invalid staff ID.");
                return false;
            }

            if (engine.getUserList().getUser(staffID) == null)
            {
                Console.WriteLine("Staff member not found.");
                return false;
            }

            Console.Write("Enter room name: ");

            if (!Enum.TryParse(Console.ReadLine(), out roomName))
            {
                Console.WriteLine("Invalid room name.");
                return false;
            }

            newSlot = new Slot(roomName, dateTime, engine.getUserList().getUser(staffID));
            if (!checkNumberOfBookings(newSlot))
            {
                return false;
            }

            if (!checkNumberOfRoomBookings(newSlot))
            {
                return false;
            }

            if (!checkAvailability(newSlot))
            {
                return false;
            }

            add(newSlot);
            Console.WriteLine("\nSlot created successfully.");
            return true;
        }

        public bool removeSlot(ASREngine engine)
        {
            int counter = 1;
            string choiceString;
            int choice;

            var allSlots =
                from s in slots
                select s;

            foreach (Slot slot in allSlots)
            {
                Console.WriteLine("    {0}. {1}", counter++, slot.ToString());
            }

            Console.WriteLine("0. Cancel");
            Console.Write("Which slot would you like to remove? Enter the number: ");

            choiceString = Console.ReadLine();

            if (!int.TryParse(choiceString, out choice))
            {
                Console.WriteLine("Invalid option.");
                return false;
            }

            if (choice == 0)
            {
                return false;
            }

            // Cannot cancel a slot if it is already booked
            if (slots[choice - 1].booking != null)
            {
                Console.WriteLine("Cannot cancel booked slot.");
            }

            slots.Remove(slots[choice - 1]);

            Console.WriteLine("Slot successfully removed");
            return true;
        }
    }
}
